﻿using AutoMapper;
using BLL.Interfaces;
using DAL1.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task23_Advanced.Models;

namespace BLL.Services
{
    public class ReviewsService:IReviewsService
    {
        IUnitOfWork Database { get; set; }

        public ReviewsService(IUnitOfWork uow)
        {
            Database = uow;
        }
        public IEnumerable<ReviewDTO> GetReviews()
        {
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<Review, ReviewDTO>()).CreateMapper();
            return mapper.Map<IEnumerable<Review>, List<ReviewDTO>>(Database.Reviews.GetAll());
        }
    }
}
