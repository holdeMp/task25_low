﻿using AutoMapper;
using BLL.DTO;
using BLL.Interfaces;
using DAL1.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task23_Advanced.Models;

namespace BLL.Services
{
    public class ArticlesService : IArticlesService
    {
        IUnitOfWork Database { get; set; }

        public ArticlesService(IUnitOfWork uow)
        {
            Database = uow;
        }
        public IEnumerable<ArticleDTO> GetArticles()
        {
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<Article, ArticleDTO>()).CreateMapper();
            return mapper.Map<IEnumerable<Article>, List<ArticleDTO>>(Database.Articles.GetAll());
        }
    }
}
