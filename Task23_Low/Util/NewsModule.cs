﻿using BLL.Interfaces;
using BLL.Services;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Task23_Advanced.Util
{
    public class NewsModule : NinjectModule
    {
        public override void Load()
        {
            Bind<INewsService>().To<NewsService>();
        }
    }
}