﻿using AutoMapper;
using BLL;
using BLL.Interfaces;
using BLL.Services;
using DAL1.Models;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Task23_Advanced.Models;

namespace Task23_Advanced.Controllers
{
    public class GuestController : Controller
    {
        private IReviewsService reviewsService;
        public GuestController(IReviewsService serv)
        {
            reviewsService = serv;
        }
        // GET: Guest
        public ActionResult Index(int? page)
        {
            IEnumerable<ReviewDTO> reviewsDtos = reviewsService.GetReviews();
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<ReviewDTO, ReviewViewModel>()).CreateMapper();
            var reviews = mapper.Map<IEnumerable<ReviewDTO>, List<ReviewViewModel>>(reviewsDtos);
            int pageSize = 3;
            int pageNumber = (page ?? 1);
            return View(reviews.ToPagedList(pageNumber, pageSize));
        }
    }
}