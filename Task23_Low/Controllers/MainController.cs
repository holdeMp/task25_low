﻿using AutoMapper;
using BLL.DTO;
using BLL.Interfaces;
using PagedList;
using DAL1.Models;
using System.Collections.Generic;
using System.Web.Mvc;
using Task23_Advanced.Models;

namespace Task23_Advanced.Controllers
{
    public class MainController : Controller
    {
        private IArticlesService articlesService;
        public MainController(IArticlesService serv)
        {
            articlesService = serv;
        }
        // GET: Main
        public ActionResult Index(int? page)
        {

            IEnumerable<ArticleDTO> articlesDtos = articlesService.GetArticles();
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<ArticleDTO, ArticleViewModel>()).CreateMapper();
            var articles = mapper.Map<IEnumerable<ArticleDTO>, List<ArticleViewModel>> (articlesDtos);
            var dividedArticles =new List<DividedArticlesViewModel>();
            foreach(var article in articles)
            {
                var lessContent = article.Content.Substring(0,199);
                var moreContent = article.Content.Substring(199);
                dividedArticles.Add(new DividedArticlesViewModel
                {
                    Date = article.Date,
                    LessContent = lessContent,
                    MoreContent = moreContent,
                    AuthorName = article.Name,
                    Id = article.Id
                }) ;
            }
            int pageSize = 3;
            int pageNumber = (page ?? 1);
            return View(dividedArticles.ToPagedList(pageNumber, pageSize));
        }
    }
}