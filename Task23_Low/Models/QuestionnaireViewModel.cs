﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Task23_Advanced.Models
{
    public class QuestionnaireViewModel
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public string Adress { get; set; }
        public bool? Scales { get; set; }
        public bool? Horns { get; set; }
        public bool? City { get; set; }
        public bool? Work { get; set; }

        public string Email { get; set; }
        public string Organisation { get; set; }
        public string Region { get; set; }
        public string Country { get; set; }
        public string Experience { get; set; }
        public string Position { get; set; }
    }
}