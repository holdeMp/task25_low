﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Microsoft.CSharp.RuntimeBinder;
using System.Web.Mvc;
using Task23_Advanced.Controllers;
using DAL1.Models;
using Task23_Advanced.Models;
using System.Collections.Generic;
using System.Linq;

namespace LibraryTestsProject
{
    [TestClass]
    public class LibraryTestsClass
    {
        [TestMethod]
        public void MainControllerTest_ReturnViewMustBeNotNull()
        {
            var controller = new MainController();
            var result = controller.Index() as ViewResult;
            Assert.IsNotNull(result);
        }
        [TestMethod]
        public void MainControllerReturnViewTest_IndexShouldReturnCorrectViewName()
        {
            var controller = new MainController();
            var result = controller.Index() as ViewResult;
            Assert.AreEqual("",result.ViewName);
        }

        [TestMethod]
        public void GuestControllerReturnViewTest_IndexShouldReturnCorrectViewName()
        {
            var controller = new GuestController();
            var result = controller.Index() as ViewResult;
            Assert.AreEqual("", result.ViewName);
        }
        [TestMethod]
        public void QuestionnaireControllerReturnViewTest_IndexShouldReturnCorrectViewName()
        {
            var controller = new QuestionnaireController();
            var result = controller.Index() as ViewResult;
            Assert.AreEqual("", result.ViewName);
        }
        [TestMethod]
        public void QuestionnaireControllerReturnViewTest_IndexShouldReturnCorrectViewBag()
        {
            var controller = new QuestionnaireController();
            var result = controller.Index() as ViewResult;

            var Scales = result.ViewBag.Scales;
            Assert.AreEqual(false, Scales);
        }
        [TestMethod]
        public void ArticlesRepositoryTest_GetAllMethodShouldReturnCorrectArticle()
        {
            var context = new Library1Context();
            var repository = new ArticleRepository(context);
            var articlesFromMethod = repository.GetAll().ToList();
            var articles = new List<Article>
            {
                new Article {Name="Google Chrome",Id=1,Date=new DateTime(2006,8,22), Content="Google Chrome is a web browser developed by Google, released in 2008. Chrome is the world's most popular web browser today!Chrome is based on the open-source code of the Chromium project, but Chrome itself is not open-source. The first beta version of Chrome was released on September 2, 2008, for personal computers (PCs) running various versions of Microsoft Corporation’s Windows OS (operating system). The development of Chrome was kept a well-guarded secret until a Web-based “comic book” describing the browser was released just hours before links appeared on Google’s Web site to download the program. " +
                "In its public statements the company declared that it did not expect to supplant the major browsers, such as Microsoft’s Internet Explorer and Firefox (the latter an open-source browser that Google supports with technical and monetary help). Instead, Google stated that its goal was to advance the usefulness of the Internet by including features that would work better with newer Web-based technologies, such as the company’s Google Apps (e.g., calendar, word processor, spreadsheet), that operate within a browser. This concept is often called “cloud computing,” as the user relies on programs operating “out there,” somewhere “in the cloud” (on the Internet)." },
                new Article {Name= "Mozilla Firefox",Id=2,Date=new DateTime(2021,1,20),Content=
                "Mozilla Firefox is an open-source web browser developed by Mozilla. Firefox has been the second most popular web browser since January, 2018.Mozilla Firefox or simply Firefox, is a free and " +
                "open-source web browser developed by the Mozilla Foundation and its subsidiary, the Mozilla Corporation. Firefox uses the Gecko rendering engine to display web pages. In 2017, Firefox began incorporating new technology under the code name Quantum to promote parallelism and a more intuitive user interface. Firefox is available for Windows 7 or Windows 10, macOS, and Linux. Its unofficial ports are available for various Unix and Unix-like operating systems including FreeBSD, OpenBSD, NetBSD, illumos, and Solaris Unix. Firefox is also available for Android and iOS. However, the iOS version uses the WebKit layout engine instead of Gecko due to platform requirements, as with all other iOS web browsers. An optimized version of Firefox is also available on the Amazon Fire TV, " +
                "as one of the two main browsers available with Amazon's Silk Browser. Firefox was created in 2002 under the code name"+ "Phoenix"+" by the Mozilla community members who desired a standalone browser, rather than the Mozilla Application Suite bundle. During its beta phase, Firefox proved to be popular with its testers and was praised for its speed, security, and add-ons compared to Microsoft's then-dominant Internet Explorer 6. Firefox was " +
                "released on November 9, 2004, and challenged Internet Explorer's dominance with 60 million " +
                "downloads within nine months. Firefox is the spiritual successor of Netscape Navigator, as " +
                "the Mozilla community was created by Netscape in 1998 before their acquisition by AOL."},
                new Article {Name= "Microsoft Edge",Id=3,Date=new DateTime(2021,5,24),Content=
                "Microsoft Edge is a web browser developed by Microsoft, released in 2015. Microsoft Edge " +
                "replaced Internet Explorer. Microsoft Edge is a cross-platform web browser created and " +
                "developed by Microsoft. It was first released for Windows 10 and Xbox One in 2015, for " +
                "Android and iOS in 2017, for macOS in 2019, and as a preview for Linux in October 2020, " +
                "and can replace Internet Explorer 11 on Windows 8.1, Windows Server 2012 R2, Windows Server " +
                "2016 and Windows Server 2019 but unlike IE11, this version does not support Windows Vista " +
                "or an earlier version. Edge was initially built with Microsoft's own proprietary browser " +
                "engine EdgeHTML and their Chakra JavaScript engine, a version now referred to as Microsoft " +
                "Edge Legacy. In 2019, Microsoft announced plans to rebuild the browser as Chromium-based " +
                "with Blink and V8 engines. During development (codenamed Anaheim), Microsoft made preview " +
                "builds of Edge available on Windows 7, 8/8.1, 10, and macOS. Microsoft announced the public " +
                "release of the new Edge on January 15, 2020. In June 2020, Microsoft began automatic " +
                "rollout of the new version via Windows Update for Windows 7, 8.1, and Windows 10 versions " +
                "from 2003 to 2004. Microsoft stopped releasing security patches for Edge Legacy from March " +
                "9, 2021, and released a security update on April 13, 2021, which replaced Edge Legacy with " +
                "Chromium-based Edge. Microsoft released the Chromium-based Edge to the Xbox Insider Alpha " +
                "Skip Ahead group on March 6, 2021."},
                  new Article { Name="Lorem ipsum dolor sit amet.",Id=4,Date = new DateTime(2021,5,26),Content=
                "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Natus, temporibus, assumenda laborum fugit reiciendis eaque in nemo aut eum cupiditate, enim dolor deleniti? Amet, nisi perferendis voluptate veritatis officiis hic sapiente accusantium est voluptatem dolor? Odio in animi dolores quas natus debitis tenetur similique ex tempora reiciendis neque ipsum quod commodi minus rerum doloremque laborum distinctio assumenda, est optio provident quos odit? Animi, voluptatum! Quos iste voluptas incidunt! Sed temporibus similique deleniti amet nisi aut inventore, quis velit necessitatibus porro deserunt exercitationem facere sunt eligendi animi, veniam accusamus rerum. Tempora impedit amet cumque! Deserunt dolorem nisi vel quia veritatis quos reiciendis excepturi facilis, quae recusandae nulla adipisci dolorum ex eius molestiae provident impedit consequuntur modi qui, illum exercitationem? Optio voluptatem expedita incidunt inventore tenetur ut saepe voluptatibus exercitationem facere, iure pariatur? Nulla, quibusdam, molestiae commodi eveniet facilis excepturi labore dolorum placeat atque alias, ullam fugit amet expedita eligendi id fugiat ea repellendus illum distinctio error. Cum incidunt impedit quo, ipsa fugit minima nobis, vero beatae ad dignissimos facere sapiente labore ipsum natus repellendus suscipit sed tenetur sint. Labore cumque facere quas nobis aliquid? Esse quam rem eligendi, ratione tenetur eaque. Autem, quae. Eius facere quibusdam sunt incidunt quas deleniti sint?" },
                  new Article { Name = "Lorem ipsum dolor sit amet consectetur adipisicing.",Id=5,Date = new DateTime(2021,7,12),
                Content = "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Minima vero sequi quibusdam, adipisci non nesciunt sint aliquid consequatur numquam tenetur possimus sunt praesentium ipsa alias cumque nostrum blanditiis eius mollitia ad repudiandae voluptatibus animi qui labore similique? Similique quo necessitatibus aperiam cum ea doloribus laborum iste, facere voluptatibus repellat dolore atque nisi dolorem, molestias quae, velit fugiat. In dolorem possimus eveniet cumque dicta nisi doloribus quas non! Eum pariatur, perferendis tempore facere et libero alias aperiam quos aliquam hic odio dolor ipsam sed repellendus quod vero voluptate velit quo amet adipisci quis eveniet. Repudiandae esse minus sequi magni natus doloremque error laboriosam qui. Laborum, voluptatibus sint nisi ad repellendus quae, similique illum architecto labore, vel quisquam eligendi est quibusdam harum! Consequatur beatae tempore est. Suscipit molestias, assumenda molestiae numquam laborum, aspernatur alias reprehenderit rem quibusdam illo placeat doloremque recusandae voluptas corrupti minus iusto minima delectus corporis eveniet animi? At tempora quas fuga culpa aliquid? Temporibus cum accusantium reprehenderit neque assumenda ducimus quae voluptatum iure? Illum alias praesentium molestiae. Quisquam voluptatibus mollitia cumque aspernatur corrupti rem perferendis magnam sed laboriosam, deleniti voluptate pariatur nemo. Vel, ducimus ad. Neque ad voluptate voluptatibus nostrum excepturi iste nisi, in aperiam consequuntur eaque eligendi distinctio, molestiae quidem nihil dolor, repudiandae hic quibusdam nulla ipsam! Doloribus dolor excepturi libero fugiat amet, alias iste tempore quaerat ullam doloremque suscipit perferendis asperiores quia nisi. Earum illo nulla saepe cum natus. Voluptatum officia impedit doloremque obcaecati corporis amet debitis veniam, odio commodi omnis soluta consequuntur eum quos nihil cumque delectus dignissimos. Dolores, eveniet error. Similique deserunt voluptates illum explicabo expedita sint ut quo neque minus molestias doloremque at amet exercitationem temporibus voluptatibus rerum, animi assumenda. Cumque sit, et amet vel sapiente adipisci officiis id aliquid voluptates quis, incidunt aspernatur ad accusamus. Id, est sit libero labore corrupti ipsa totam animi eligendi adipisci eos minus itaque inventore quaerat saepe cupiditate ipsum porro soluta vero ullam. Exercitationem placeat aperiam minima harum quisquam fugiat aspernatur sunt id, assumenda accusamus impedit quam dicta sequi commodi consequatur sit a suscipit illo, itaque molestiae? Obcaecati commodi optio, explicabo veritatis pariatur quisquam architecto minus non corrupti?" },

            };
            for (int i = 0;i<articles.Count;i++)
            {
                CompareArticles(articles[i], articlesFromMethod[i]);
            }
        }
        [TestMethod]
        public void ReviewsRepositoryTest_GetAllMethodShouldReturnCorrectReviews()
        {
            var context = new Library1Context();
            var repository = new ReviewsRepository(context);
            var ReviewsFromMethod = repository.GetAll().ToList();
            var reviews = new List<Review>
            {
               new Review {AuthorName="Lorem, ipsum.",Date=new DateTime(2000,8,22),Content="Lorem ipsum dolor sit amet consectetur, adipisicing elit. Eos dolores nobis, ipsa est voluptate accusantium assumenda accusamus necessitatibus eligendi itaque enim voluptates quae reiciendis maiores impedit quia cumque quas ducimus."},
               new Review {AuthorName="Lorem, ipsum.",Date=new DateTime(2000,8,23),Content="Lorem ipsum dolor sit amet consectetur, adipisicing elit. Eos dolores nobis, ipsa est voluptate accusantium assumenda accusamus necessitatibus eligendi itaque enim voluptates quae reiciendis maiores impedit quia cumque quas ducimus."},
               new Review {AuthorName="Lorem, ipsum.",Date=new DateTime(2000,8,24),Content="Lorem ipsum dolor sit amet consectetur, adipisicing elit. Eos dolores nobis, ipsa est voluptate accusantium assumenda accusamus necessitatibus eligendi itaque enim voluptates quae reiciendis maiores impedit quia cumque quas ducimus."},
               new Review {AuthorName="Lorem, ipsum.",Date=new DateTime(2000,8,25),Content="Lorem ipsum dolor sit amet consectetur, adipisicing elit. Eos dolores nobis, ipsa est voluptate accusantium assumenda accusamus necessitatibus eligendi itaque enim voluptates quae reiciendis maiores impedit quia cumque quas ducimus."},
               new Review {AuthorName="Lorem, ipsum.",Date=new DateTime(2000,8,26),Content="Lorem ipsum dolor sit amet consectetur, adipisicing elit. Eos dolores nobis, ipsa est voluptate accusantium assumenda accusamus necessitatibus eligendi itaque enim voluptates quae reiciendis maiores impedit quia cumque quas ducimus."},
               new Review {AuthorName="Lorem, ipsum.",Date=new DateTime(2000,8,27),Content="Lorem ipsum dolor sit amet consectetur, adipisicing elit. Eos dolores nobis, ipsa est voluptate accusantium assumenda accusamus necessitatibus eligendi itaque enim voluptates quae reiciendis maiores impedit quia cumque quas ducimus."},
               new Review {AuthorName="Lorem, ipsum.",Date=new DateTime(2000,8,28),Content="Lorem ipsum dolor sit amet consectetur, adipisicing elit. Eos dolores nobis, ipsa est voluptate accusantium assumenda accusamus necessitatibus eligendi itaque enim voluptates quae reiciendis maiores impedit quia cumque quas ducimus."},

            };
            for (int i = 0; i < reviews.Count; i++)
            {
                CompareReviews(reviews[i], ReviewsFromMethod[i]);
            }
        }
        [TestMethod]
        public void NewsRepositoryTest_GetAllMethodShouldReturnCorrectNews()
        {
            var context = new Library1Context();
            var repository = new NewsRepository(context);
            var NewsFromMethod = repository.GetAll().ToList();
            var news = new List<News>
            {
                new News {Title="Lorem ipsum",Content = "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Hic delectus ex numquam nulla, dignissimos maiores et aperiam totam repellendus doloribus voluptatem asperiores similique nostrum voluptatum error, quo odit ullam sapiente eius voluptas laudantium. Sed est perferendis cum minus aliquam, velit veritatis fugiat molestias neque, porro optio illum, ipsum corrupti? Amet aspernatur eius a tempora at doloribus qui deserunt cumque, itaque nam! Iste asperiores tempore similique voluptatem ut inventore fugiat consequatur! Quae quod, dolores excepturi labore at tenetur. Facere magnam soluta omnis quae reiciendis quibusdam ipsam dolore ea voluptates. Quos expedita cumque labore tempora eos nihil reprehenderit sapiente vitae adipisci, quas unde impedit blanditiis quidem commodi minus molestiae dolore laborum quo culpa? Mollitia exercitationem rerum, commodi officia optio officiis totam minus in corporis, pariatur excepturi ad. Consequatur perferendis dolorem veritatis eligendi provident ab vel doloribus impedit harum, laboriosam molestiae iste repellendus asperiores est nihil fugit magnam animi tenetur in numquam saepe labore! Quas eligendi alias quasi nesciunt, distinctio dolor et minima similique odio animi repellat quis fuga laboriosam, reiciendis quidem ex fugit illo perspiciatis quos ea! Doloribus corporis blanditiis voluptatem tenetur, doloremque numquam iste, veniam qui cumque iusto, optio omnis eligendi magnam autem eveniet est. Eum ratione officiis eligendi commodi placeat dolores nihil repellendus nemo consequatur consectetur mollitia impedit veniam doloribus, numquam voluptas veritatis, et omnis distinctio neque at iste maxime? Cupiditate dicta ratione velit harum beatae facilis ea laudantium voluptates corporis impedit ducimus excepturi at delectus laborum ex unde nam, quibusdam est pariatur earum quidem. Ex possimus corrupti omnis quis incidunt saepe, iure nesciunt deserunt earum aperiam sit tempora? Culpa tempora ad libero. Nihil impedit et consequatur nostrum! Quia delectus quam ea dicta illum vero velit cupiditate quae modi similique tempora eius dolorem ad, quas, atque facilis mollitia adipisci fuga. Soluta odit cupiditate dicta tenetur vel voluptatibus maiores veritatis explicabo?"}

            };
            for (int i = 0; i < news.Count; i++)
            {
                CompareNews(news[i], NewsFromMethod[i]);
            }
        }
        public static void CompareNews(News expected, News actual)
        {
            Assert.AreEqual(expected.Title, actual.Title);
            Assert.AreEqual(expected.Content, actual.Content);
        }
        public static void CompareReviews(Review expected, Review actual)
        {
            Assert.AreEqual(expected.AuthorName, actual.AuthorName);
            Assert.AreEqual(expected.Date, actual.Date);
            Assert.AreEqual(expected.Content, actual.Content);
        }
        public static void CompareArticles(Article expected, Article actual)
        {
            Assert.AreEqual(expected.Name, actual.Name);
            Assert.AreEqual(expected.Date, actual.Date);
            Assert.AreEqual(expected.Id, actual.Id);
            Assert.AreEqual(expected.Content, actual.Content);
        }
    }
}
