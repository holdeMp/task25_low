﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Task23_Advanced.Validation
{
    /// <summary>
    /// Custom Article Attribute to validate ArticleDate to be no berofe than Year parameter
    /// </summary>
    /// <param name="year">
    /// Year to ArticleDate no be berofe than
    /// </param>
    public class ArticleDateAttribute : ValidationAttribute
    {
        public ArticleDateAttribute(int year)
        {
            Year = year;
        }

        public int Year { get; }

        public string GetErrorMessage() =>
            $"Reviews must have a release year no later than {Year}.";

        protected override ValidationResult IsValid(object value,
            ValidationContext validationContext)
        {
            var releaseYear = ((DateTime)value).Year;

            if (releaseYear < Year)
            {
                return new ValidationResult(GetErrorMessage());
            }

            return ValidationResult.Success;
        }
    }
}