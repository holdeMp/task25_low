﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Task23_Advanced.Models;

namespace Task23_Advanced.Validation
{
    /// <summary>
    /// Custom Review Attribute to validate ReleaseDate to be no berofe than Year parameter
    /// </summary>
    /// <param name="year">
    /// Year to ReleaseDate no be berofe than
    /// </param>
    public class ReviewAttribute : ValidationAttribute
    {
        public ReviewAttribute(int year)
        {
            Year = year;
        }

        public int Year { get; }

        public string GetErrorMessage() =>
            $"Reviews must have a release year no later than {Year}.";

        protected override ValidationResult IsValid(object value,
            ValidationContext validationContext)
        {
            var releaseYear = ((DateTime)value).Year;

            if (releaseYear < Year)
            {
                return new ValidationResult(GetErrorMessage());
            }

            return ValidationResult.Success;
        }
    }
}