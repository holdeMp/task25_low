﻿using DAL1.Interfaces;
using DAL1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task23_Advanced.Models;

namespace DAL1.Repositories
{
    public class EFUnitOfWork : IUnitOfWork
    {
        private Library1Context db;
        private ReviewsRepository _reviewsRepository;
        private ArticleRepository _articleRepository;
        private NewsRepository _newsRepository;
        public EFUnitOfWork(string connectionString)
        {
            db = new Library1Context(connectionString);
        }
        public IRepository<Article> Articles
        {
            get
            {
                if (_articleRepository == null)
                    _articleRepository = new ArticleRepository(db);
                return _articleRepository;
            }
        }
        public IRepository<Review> Reviews
        {
            get
            {
                if (_reviewsRepository == null)
                    _reviewsRepository = new ReviewsRepository(db);
                return _reviewsRepository;
            }
        }

        public IRepository<News> News
        {
            get
            {
                if (_newsRepository == null)
                    _newsRepository = new NewsRepository(db);
                return _newsRepository;
            }
        }

        public void Save()
        {
            db.SaveChanges();
        }

        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
                this.disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
