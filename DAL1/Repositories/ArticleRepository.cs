﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task23_Advanced.Models;

namespace DAL1.Models
{
    public class ArticleRepository : IRepository<Article>
    {
        private readonly Library1Context _db;
        public ArticleRepository(Library1Context context)
        {
            this._db = context;
        }
        public IEnumerable<Article> GetAll()
        {
            return _db.Articles.ToList();
        }
    }
}
