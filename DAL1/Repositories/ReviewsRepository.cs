﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task23_Advanced.Models;

namespace DAL1.Models
{
    public class ReviewsRepository : IRepository<Review>
    {
        private readonly Library1Context _db;
        public ReviewsRepository(Library1Context context)
        {
            this._db = context;
        }
        public IEnumerable<Review> GetAll()
        {
            return _db.Reviews.ToList();
        }
    }
}
